const data = fetch("../data.json")
  .then(res => res.json())
  .then(data => {
    // Extract Cards data from JSON
    const cards = [];
    data.forEach(cardData => {
      const card = {
        id: cardData.id,
        name: cardData.name,
        image: cardData.image,
        caption: cardData.caption,
        type: cardData.type,
        sourceType: cardData.source_type,
        sourceLink: cardData.source_link,
        date: cardData.date,
        likes: cardData.likes,
        profileImage: cardData.profile_image,
      };
      cards.push(card);
    });

    console.log(data)

    const numCardsToShow = 4; // Number of cards to show on each load
    let currentCardsToShow = numCardsToShow; // Current number of cards to show
    let filterBySource = "all"; // Initially display all sources

    // Generate HTML for each card
    const generateCardHTML = (card) => {
      return `
        <div class="cards-container">  
          <div class="card">
          <div class="card-wrapper">
          <div class="card-header">
          <img src="${card.profileImage}" alt="${card.name}" class="rounded-circle" width="30">
          <div class="card-header-info">
          <h5 class="card-title"><strong>${card.name}</strong></h5>
          <p class="card-date">${card.date}</p>
          </div>
          </div>
          <div class="card-logo">
          <svg width="25" height="20" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
           <path d="M7.98371 0.0333252C3.57448 0.0333252 0 3.6078 0 8.01704C0 11.9716 2.87829 15.2467 6.65219 15.8809V9.6827H4.72629V7.45218H6.65219V5.80751C6.65219 3.89923 7.81771 2.85932 9.52029 2.85932C10.3357 2.85932 11.0365 2.92009 11.2399 2.94685V4.94151L10.059 4.94209C9.13333 4.94209 8.95486 5.3819 8.95486 6.02751V7.45104H11.1637L10.8756 9.68155H8.95486V15.9342C12.905 15.4535 15.9673 12.095 15.9673 8.01475C15.9673 3.6078 12.3929 0.0333252 7.98371 0.0333252Z" fill="#1778F2"/>
          </svg>
          </div>
          </div>
          <img class="card-img-top" src="${card.image}" alt="${card.caption}">
            <div class="card-body">
              <p class="card-text">${card.caption}</p>     
            </div>
            <hr class="line">
            <div class="card-footer">
            <svg class="likes"width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M14.7617 3.26543C14.3999 2.90347 13.9703 2.61634 13.4976 2.42045C13.0248 2.22455 12.518 2.12372 12.0063 2.12372C11.4945 2.12372 10.9878 2.22455 10.515 2.42045C10.0422 2.61634 9.61263 2.90347 9.25085 3.26543L8.50001 4.01626L7.74918 3.26543C7.0184 2.53465 6.02725 2.1241 4.99376 2.1241C3.96028 2.1241 2.96913 2.53465 2.23835 3.26543C1.50756 3.99621 1.09702 4.98736 1.09702 6.02084C1.09702 7.05433 1.50756 8.04548 2.23835 8.77626L2.98918 9.52709L8.50001 15.0379L14.0108 9.52709L14.7617 8.77626C15.1236 8.41448 15.4108 7.98492 15.6067 7.51214C15.8026 7.03935 15.9034 6.53261 15.9034 6.02084C15.9034 5.50908 15.8026 5.00233 15.6067 4.52955C15.4108 4.05677 15.1236 3.62721 14.7617 3.26543V3.26543Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
              <small class="text-muted">${card.likes} likes</small>
            </div>
          </div>
        </div>`;
    };

    // Insert the HTML into the DOM
    const cardsContainer = document.getElementById('cards');

    const showCards = () => {
    let filteredCards = cards.filter((card) => filterBySource === "all" || card.sourceType === filterBySource);
    const cardsToShow = filteredCards.slice(0, currentCardsToShow);
    const cardHTML = cardsToShow.map((card) => generateCardHTML(card));
    cardsContainer.innerHTML = cardHTML.join('');

      // "Load More" button if there are more cards to show
      if (currentCardsToShow < filteredCards.length) {
        const loadMoreBtn = document.createElement('button');
        loadMoreBtn.innerText = 'Load More';
        loadMoreBtn.classList.add('load-more-btn');
        loadMoreBtn.addEventListener('click', () => {
        currentCardsToShow += numCardsToShow;
        showCards();
          if (currentCardsToShow >= filteredCards.length) {
            loadMoreBtn.style.display = 'none';
          }
        });
        cardsContainer.appendChild(loadMoreBtn);
      }
    };
    let filteredCards = []
      if (currentCardsToShow < filteredCards.length) {
      const loadMoreBtn = document.createElement('button');
      loadMoreBtn.innerText = 'Load More';
      loadMoreBtn.classList.add('load-more-btn');
      loadMoreBtn.addEventListener('click', () => {
        currentCardsToShow += numCardsToShow;
        showCards();
        if (currentCardsToShow >= filteredCards.length) {
          loadMoreBtn.style.display = 'none';
        }
      });
      cardsContainer.appendChild(loadMoreBtn);
    }

    // Filter by sourceType event listener
    const filterRadioBtns = document.getElementsByName('filterBySource');
    filterRadioBtns.forEach((btn) => {
      btn.addEventListener('change', (event) => {
        filterBySource = event.target.value;
        currentCardsToShow = numCardsToShow;
        showCards();
      });
    });

    // dark theme; light theme
    const toggleButton = document.querySelector(".dark-theme");
    const toggleButtonLight = document.querySelector(".light-theme");
   
    // const cardsContainer = document.querySelector(".cards-container");
      toggleButton.addEventListener("change", () => {
      cardsContainer.classList.toggle("dark-theme");
    });
    
    toggleButtonLight.addEventListener("change", () => {
      cardsContainer.classList.toggle("light-theme");
    });
    
    showCards();
  });
  
  const cardsContainer = document.getElementById('cards');

const colorInput = document.getElementById('cardBackgroundColor');
colorInput.addEventListener('input', () => {
  const newColor = colorInput.value;
  cardsContainer.style.backgroundColor = newColor;
});

const cardBackgroundColorInput = document.getElementById('cardBackgroundColor');
cardBackgroundColorInput.addEventListener('input', (event) => {
  const newColor = event.target.value;
  cardsContainer.style.setProperty('--card-bg-color', newColor);
});

const cardSpaceBetween = document.getElementById("cardSpaceBetween");

// Update gap value when input field changes
cardSpaceBetween.addEventListener("input", (event) => {
  const gap = event.target.value + "px";
  document.getElementById("cards").style.gap = gap;
});
